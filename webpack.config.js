
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        bundle: path.resolve(__dirname, 'src/JS/index.js'),
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name][contenthash].js',
        clean: true
    },
    devServer: {
        static: path.resolve(__dirname, 'src/'),
        port: 3001,
        hot: true,
        open: true,
        compress: true,
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
        },
        {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                }
            }
        },
        {
            test: /\.html$/i,
            loader: "html-loader",
        },
        {
            test: /\.(png|jpg|jpeg|svg|gif)/i,
            type: 'asset/resource',
            generator: {
                filename: 'images/[name][contenthash][ext]',
            },
        },
        {
            test: /\.(woff|woff2)$/i,
            type: 'asset/resource',
            generator: {
                filename: 'fonts/[name][ext]',
            }
        },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html',
        })
    ],
}

