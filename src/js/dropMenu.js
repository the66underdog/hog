import { btnBurger, navMenu, navMenuCopy } from './selectors';

function hideNavMenu(e) {
    e.currentTarget.removeAttribute('style');
    e.currentTarget.removeEventListener('animationend', hideNavMenu);
}

function toggleMenu(e) {
    if (e.target.closest('.button-burger_dropped')) {
        e.currentTarget.classList.remove('button-burger_dropped');
        navMenu.style.animation = 'fadeAway 0.66s';
        navMenu.addEventListener('animationend', hideNavMenu);
    }
    else {
        e.currentTarget.classList.add('button-burger_dropped');
        navMenu.style.cssText = `
            visibility: visible;
            opacity: 1;
        `;
    }
}

btnBurger.addEventListener('click', toggleMenu);