import { chipLine } from "./selectors";

window.onload = () => {
    for (let item of chipLine) {
        item.style.animationDirection = Math.random() < 0.5 ? 'normal' : 'reverse';
    }
}