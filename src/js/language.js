import { btnLanguage } from './selectors';

function toggleLanguage(e) {
    e.currentTarget.lastElementChild.innerHTML = e.currentTarget.lastElementChild.innerHTML === 'en' ? 'ru' : 'en';
}

btnLanguage.addEventListener('click', e => toggleLanguage(e));